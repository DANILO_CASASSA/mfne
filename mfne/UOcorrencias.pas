unit UOcorrencias;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Layouts,
  FMX.ScrollBox, FMX.Memo;

type
  TfrmOcorrencias = class(TForm)
    lytTopOcorrencias: TLayout;
    lytBotOcorrencias: TLayout;
    lytTop: TGridPanelLayout;
    imgLogoMFNE: TImage;
    lbNomeTelaOcorrencias: TLabel;
    bgTopOcorrencias: TImage;
    bgBotOcorrencias: TImage;
    lytBot: TGridPanelLayout;
    lbInfoGeraisOcorrencias: TLabel;
    btnVoltar: TImage;
    lbBtnVoltar: TLabel;
    lytConteudo: TLayout;
    ScrollOcorrencias: TVertScrollBox;
    motOcorrencia1: TMemo;
    lbOcorrencia1: TLabel;
    lytOcorrencia1: TLayout;
    lytOcorrencia3: TLayout;
    lytOcorrencia2: TLayout;
    lbOcorrencia3: TLabel;
    motOcorrencia3: TMemo;
    lbOcorrencia2: TLabel;
    motOcorrencia2: TMemo;
    procedure btnVoltarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOcorrencias: TfrmOcorrencias;

implementation

{$R *.fmx}
{$R *.SmXhdpiPh.fmx ANDROID}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UMain;

procedure TfrmOcorrencias.btnVoltarClick(Sender: TObject);
begin
    frmOcorrencias.Hide;
    frmMain.show;
end;

procedure TfrmOcorrencias.FormCreate(Sender: TObject);
begin
    motOcorrencia1.Text := 'Ocorr�ncia n�o registrada.';
    motOcorrencia2.Text := 'Ocorr�ncia n�o registrada.';
    motOcorrencia3.Text := 'Ocorr�ncia n�o registrada.';
end;

end.
