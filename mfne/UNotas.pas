unit UNotas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Layouts, FMX.Controls.Presentation, FMX.StdCtrls, System.Rtti,
  FMX.ListBox, FMX.Grid;

type
  TfrmNotas = class(TForm)
    lytTopNotas: TLayout;
    lytLogo: TLayout;
    lytNomeTelaNotas: TLayout;
    lytBotomNotas: TLayout;
    lytEscolherAluno: TLayout;
    lytConteudo: TLayout;
    bgTopNotas: TImage;
    imgLogoNotas: TImage;
    lbTelaNotas: TLabel;
    bgBottomNotas: TImage;
    imgVoltar: TImage;
    lbVoltar: TLabel;
    lbInfoGeralNotas: TLabel;
    gridNotas: TStringGrid;
    lytAluno: TLayout;
    lytComboxAluno: TLayout;
    lbAluno: TLabel;
    cboxAluno: TComboBox;
    lytPeriodo: TLayout;
    lbPeriodo: TLabel;
    comboxPeriodo: TComboBox;
    lytLabelPeriodo: TLayout;
    lytComboxPeriodo: TLayout;
    gridPnlNotas: TGridPanelLayout;
    procedure imgVoltarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNotas: TfrmNotas;

implementation

{$R *.fmx}
{$R *.SmXhdpiPh.fmx ANDROID}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UMain;

procedure TfrmNotas.imgVoltarClick(Sender: TObject);
begin
    frmNotas.hide;
    frmMain.show;
end;

end.
