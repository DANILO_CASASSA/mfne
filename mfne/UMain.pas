unit UMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Layouts,
  FMX.TabControl;

type
  TfrmMain = class(TForm)
    btnSairApp: TSpeedButton;
    btnGradeHoraria: TImage;
    btnNotas: TImage;
    TabControlfrmMain: TTabControl;
    tabMenuPrincipal: TTabItem;
    tabAvisos: TTabItem;
    tabSaibaMais: TTabItem;
    imgBtnSair: TImage;
    lytHeaderMain: TLayout;
    bgTopMain: TImage;
    imgLogoMFNE: TImage;
    btnOcorrencias: TImage;
    lytMenu: TLayout;
    gridPnlMenus: TGridPanelLayout;
    procedure btnSairAppClick(Sender: TObject);
    procedure btnGradeHorariaClick(Sender: TObject);
    procedure btnNotasClick(Sender: TObject);
    procedure btnOcorrenciasClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}
{$R *.SmXhdpiPh.fmx ANDROID}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

uses ULogin, UGradeHoraria, UNotas, UOcorrencias;

procedure TfrmMain.btnGradeHorariaClick(Sender: TObject);
begin
    frmMain.Hide;
    frmGradeHoraria.Show;
end;

procedure TfrmMain.btnNotasClick(Sender: TObject);
begin
    frmMain.Hide;
    frmNotas.show;
end;

procedure TfrmMain.btnOcorrenciasClick(Sender: TObject);
begin
    frmMain.Hide;
    frmOcorrencias.show;
end;

procedure TfrmMain.btnSairAppClick(Sender: TObject);
begin
    Application.Terminate;
end;

end.
