unit UGradeHoraria;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Layouts,
  System.Rtti, FMX.Grid, FMX.ListBox, FMX.Effects;

type
  TfrmGradeHoraria = class(TForm)
    lytTopAgenda: TLayout;
    lytLogoAgenda: TLayout;
    lytNomeTelaAgenda: TLayout;
    lytBtnVoltarAgenda: TLayout;
    lytBotAgendaVoltar: TLayout;
    bgTBarAgenda: TImage;
    Logo: TImage;
    lbAgenda: TLabel;
    lytBottomAgenda: TLayout;
    bgBottomAgenda: TImage;
    btnVoltarAgenda: TImage;
    lbBtnVoltar: TLabel;
    lbInfoGeralAgenda: TLabel;
    lytConteudoAgenda: TLayout;
    lytPesquisa: TLayout;
    lytConteudo: TLayout;
    Label1: TLabel;
    lytLabelPesquisa: TLayout;
    lytEscolherFIlho: TLayout;
    comboxAlunos: TComboBox;
    gridAgenda: TGrid;
    lytPeriodo: TLayout;
    cboxPeriodo: TComboBox;
    lytPeriodoEscolher: TLayout;
    lbPeriodo: TLabel;
    procedure btnVoltarAgendaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGradeHoraria: TfrmGradeHoraria;

implementation

{$R *.fmx}

uses UMain;


procedure TfrmGradeHoraria.btnVoltarAgendaClick(Sender: TObject);
begin
    frmGradeHoraria.Hide;
    frmMain.Show;
end;

end.

