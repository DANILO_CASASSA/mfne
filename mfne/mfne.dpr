program mfne;

uses
  System.StartUpCopy,
  FMX.Forms,
  ULogin in 'ULogin.pas' {frmLogin},
  UMain in 'UMain.pas' {frmMain},
  UGradeHoraria in 'UGradeHoraria.pas' {frmGradeHoraria},
  UNotas in 'UNotas.pas' {frmNotas},
  UOcorrencias in 'UOcorrencias.pas' {frmOcorrencias};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmGradeHoraria, frmGradeHoraria);
  Application.CreateForm(TfrmNotas, frmNotas);
  Application.CreateForm(TfrmOcorrencias, frmOcorrencias);
  Application.Run;
end.
