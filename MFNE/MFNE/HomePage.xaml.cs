﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MFNE
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
           this.BackgroundImage = "@drawable/telaLogin";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string CPF = editCPFLogin.Text;
            string Senha = editSenhaLogin.Text;

            this.BackgroundColor = Color.Gray;
            lbMensagem.Text = "Você informou o CPF " + CPF + " e a Senha " + Senha;

        }
    }
}
